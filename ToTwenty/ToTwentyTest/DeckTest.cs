﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using ToTwenty.src.Model;

namespace ToTwentyTest
{
    [TestClass]
    public class DeckTest
    {
        [TestMethod]
        public void GetSetPropertyTest()
        {
            //Assign
            Card card = new Card(CardType.Plus, 1);
            List<Card> listCards = new List<Card>();
            Deck deck = new Deck();

            //Act
            listCards.Add(card);
            deck.DeckCards = listCards;

            //Assert
            Assert.AreEqual(listCards, deck.DeckCards);
        }

        [TestMethod]
        public void AddCardTest()
        {
            //Assign
            Card card = new Card(CardType.Plus, 1);
            List<Card> listCards = new List<Card>();
            Deck deck = new Deck();

            //Act
            listCards.Add(card);
            deck.AddCard(card);

            //Assert
            Assert.AreEqual(listCards, deck.DeckCards);
        }
    }
}
