using Microsoft.VisualStudio.TestTools.UnitTesting;
using ToTwenty.src.Model;

namespace ToTwentyTest
{
    [TestClass]
    public class CardTest
    {
        [TestMethod]
        public void CardPropertyTest()
        {
            //Assign
            Card card = new Card(CardType.Plus, 1);
            int intValue;
            CardType cardType;

            //Act
            intValue = card.Value;
            cardType = card.CardType;

            //Assert
            Assert.AreEqual(1, intValue);
            Assert.AreEqual(CardType.Plus, cardType);
        }

        [TestMethod]
        public void CardStringTest()
        {
            //Assign
            Card card = new Card(CardType.Plus, 1);
            string strReturn;

            //Act
            strReturn = card.ReturnCardValue();

            //Assert
            Assert.AreEqual("+1", strReturn);
        }
    }
}
