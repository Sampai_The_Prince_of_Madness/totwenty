﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ToTwenty.src;
using ToTwenty.src.Model;

namespace ToTwenty
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void PlayAI_Click(object sender, RoutedEventArgs e)
        {
            Player player1 = new Player("Player");
            Player player2 = new Player("AI");

            Game game = new Game(GameType.AI, player1, player2);
            game.Show();
            this.Hide();
        }
    }
}
