﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ToTwenty.src;
using ToTwenty.src.Model;

namespace ToTwenty
{
    /// <summary>
    /// Interaction logic for Game.xaml
    /// </summary>
    public partial class Game : Window
    {
        private GameType gameType;
        public Player player1;
        public Player player2;

        public Game(GameType pGameType, Player pPlayer1, Player pPlayer2)
        {
            InitializeComponent();
            gameType = pGameType;
            player1 = pPlayer1;
            player2 = pPlayer2;

            Button button = new Button();
            ImageBrush imageBrush = new ImageBrush();
            imageBrush.ImageSource = new BitmapImage(new Uri(@"pack://application:,,,/Resources/pazaakCardExampleAndroid.jpg"));
            button.Background = imageBrush;
            button.Width = 120;
            button.Height = 150;
            //button.Click += CardClickHandler;
            //button.Content = card.ReturnCardInfo();
            wrpPlayerHand.Children.Add(button);

            button = new Button();
            button.Background = imageBrush;
            button.Width = 120;
            button.Height = 150;
            //button.Click += CardClickHandler;
            //button.Content = card.ReturnCardInfo();
            wrpPlayerHand.Children.Add(button);

            GameController game = new GameController();
        }
    }
}
