﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToTwenty.src.Model
{
    public class Card
    {
        private CardType type;
        private int value;

        public Card(CardType pType, int pValue)
        {
            type = pType;
            value = pValue;
        }

        public CardType CardType
        {
            get
            {
                return type;
            }
        }

        public int Value
        {
            get
            {
                return value;
            }
        }

        public string ReturnCardValue()
        {
            switch (type)
            {
                case CardType.Minus:
                    return "-" + Value;

                default:
                    return "+" + Value;
            }
        }
    }
}
