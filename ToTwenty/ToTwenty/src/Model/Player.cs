﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToTwenty.src.Model
{
    public class Player
    {
        private Deck playerDeck;
        private string name;
        private List<Card> playerHand;

        public Player(string pName)
        {
            name = pName;
            playerDeck = new Deck();
            playerHand = new List<Card>();
        }

        public Deck PlayerDeck
        {
            get
            {
                return playerDeck;
            }

            set
            {
                playerDeck = value;
            }
        }
    }
}
