﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToTwenty.src.Model
{
    public class Deck
    {
        private List<Card> deckCards;

        public Deck()
        {
            deckCards = new List<Card>();
        }

        public List<Card> DeckCards
        {
            get
            {
                return deckCards;
            }

            set
            {
                deckCards = value;
            }
        }

        public void AddCard(Card pCard)
        {
            deckCards.Add(pCard);
        }

        public void RemoveCard(Card pCard)
        {
            deckCards.Remove(pCard);
        }
    }
}
